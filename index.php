<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Отзывы &dash; Tutor.ru</title>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<style>
    small, .note { display: block; color: #888; }

    #alert { width: 100%; }
    .section { display: block; width: 100%; margin: 40px auto; padding: 40px; border: 1px solid #eee; }
    .note { font-size: 80%; font-style: italic; }
</style>
</head>
<body>
    <?php require_once 'lib/app.php'; $app = new App; ?>

    <div class="container">
        <div class="feedback__form section">
            <div class="row">
                <h3>Оставить отзыв</h3>
            </div>

            <div class="row mb-4">
                <small>* - все поля обязательны для заполнения. Пожалуйста, проверьте правильность перед отправкой отзыва.</small>
            </div>

            <div class="row mt-4">
                <div id="alert"></div>

                <form method="post" action="/post.php" style="width: 100%;">
                    <div class="form-group">
                        <label for="name">Ваше имя</label>
                        <input type="text" name="name" class="form-control" required />
                    </div>

                    <div class="form-group">
                        <label for="email">Ваш email</label>
                        <input type="email" name="email" class="form-control" required />
                    </div>

                    <div class="form-group mb-4 pb-4">
                        <label for="feedback">Отзыв</label>
                        <textarea name="text" rows="6" class="form-control" required></textarea>
                        <span class="note mt-2">Можно использовать следующие HTML-теги и атрибуты: <?php echo htmlspecialchars( implode( ', ', $app->getAllowableTags() ) ); ?>.</span>
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-outline-primary btn-lg btn-block" value="Отправить" />
                    </div>
                </form>
            </div>
        </div>


    <?php $feedbacks = $app->feedbackGetAll(); ?>
    <?php if ( ! empty( $feedbacks ) ) : ?>

        <div class="feedback__list section">
            <div class="row">
                <h3>Отзывы</h3>
            </div>

            <div class="row">
                <table class="table table-striped">
                    <tbody>
                        <?php foreach ( $feedbacks as $feedback ) : ?>
                            <tr>
                                <td>
                                    <strong><?php echo $app->stripTags( $feedback->name ); ?><strong>
                                    <small><?php echo $app->stripTags( $feedback->email ); ?></small>
                                    <small><?php echo $feedback->created_at; ?></small>
                                </td>
                                <td><?php echo $app->feedbackMessage( $feedback->text ); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <?php endif; ?>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

    <script>
        $(function() {
            function showAlert(status, message) {
                var alert   = $('body').find('#alert'),
                    html    = '<div class=""><div class="alert alert-'+status+'">'+message+'</div>';

                $(alert).append(html);

                if (status == 'success') {
                    setTimeout(function() {
                        window.location.reload();
                    }, 2000);
                }
            }

            $('form').submit(function(e) {
                var $form = $(this);

                $('body').find('#alert').empty();

                $.ajax({
                    type    : $form.attr('method'),
                    url     : $form.attr('action'),
                    data    : $form.serialize()
                }).done(function(data) {
                    try {
                        data = JSON.parse(data);
                        showAlert(data.status, data.message);
                    } catch (err) {
                        showAlert('danger', 'Что-то пошло не так. Отзыв не был отправлен.');
                    }
                }).fail(function() {
                    showAlert('danger', 'Что-то пошло не так. Отзыв не был отправлен.');
                });

                e.preventDefault();
            });
        });
    </script>
</body>
</html>