<?php
class App {
    private $db;

    private $db_defaults = [
        'host'      => 'localhost',
        'charset'   => 'utf8mb4',
        'name'      => 'tutor_test_bse',
        'user'      => 'tutor_test_usr',
        'pass'      => '7MnX6NmjzNsXgpMo'
    ];

    private $allowable_fields = [
        'name', 'email', 'text'
    ];

    private $allowable_tags = [
        '<strong>', '<s>', '<i>', '<del>'
    ];

    public function __construct()
    {
        $this->db = new mysqli( $this->db_defaults['host'], $this->db_defaults['user'], $this->db_defaults['pass'], $this->db_defaults['name'] );

        if ( ! $this->db )
            $this->dbError( $this->db->connect_errno() . " " .$this->db->connect_error() );

        $this->db->set_charset( $this->db_defaults['charset'] );
    }

    public function feedbackGetAll()
    {
        $results = [];

        if ( $result = $this->db->query( "SELECT *, DATE_FORMAT(created_at, '%d-%m-%y %H:%i:%s') AS created_at FROM feedbacks ORDER BY created_at DESC LIMIT 20000000" ) ) {
            while ( $row = $result->fetch_object() )
                $results[] = $row;

            $result->close();
        }

        return $results;
    }

    public function feedbackPost( $name, $email, $text )
    {
        $query  = $this->db->prepare(
            "INSERT INTO feedbacks(name, email, text) VALUES (?, ?, ?)"
        );

        $query->bind_param( 'sss', $name, $email, $text );
        $query->execute();

        return $query->affected_rows ? true : false;
    }

    public function getAllowableFields()
    {
        return $this->allowable_fields;
    }

    public function getAllowableTags()
    {
        return $this->allowable_tags;
    }

    public function stripTags( $string )
    {
        return strip_tags( $string, implode( '', $this->allowable_tags ) );
    }

    public function feedbackMessage( $string )
    {
        $string = strip_tags( $string, implode( '', $this->allowable_tags ) );
        $string = nl2br( $string );

        return $string;
    }

    public function preparedMessage( $status, $message )
    {
        return json_encode( [
            'status'    => $status,
            'message'   => $message
        ], JSON_UNESCAPED_UNICODE );
    }

    protected function dbError( $error )
    {
        $error  = __CLASS__ . ": " . $error;
        $error .= ". Error initiated in " . $this->dbErrorCaller() . ", thrown";

        trigger_error( $error, E_USER_ERROR );
    }

    protected function dbErrorCaller()
    {
        $trace  = debug_backtrace();
        $caller = '';

        foreach ( $trace as $t ) {
            if ( isset( $t['class'] ) && $t['class'] == __CLASS__ )
                $caller = $t['file']." on line ".$t['line'];
            else
                break;
        }

        return $caller;
    }
}
// That's all. Bye!