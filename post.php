<?php
require_once 'lib/app.php';

$app    = new App;
$error  = false;

foreach ( $app->getAllowableFields() as $field ) {
    if ( ! isset( $_POST[$field] ) || empty ( $_POST[$field] ) ) {
        $error = true;
        break;
    }

    ${$field} = $_POST[$field];
}

if ( $error === true ) {
    echo $app->preparedMessage( 'danger', 'Не заполнено 1 или более обязательных полей.' );
    return;
}

if ( ! filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
    echo $app->preparedMessage( 'danger', 'Ошибка при заполении поля email. Адрес не валиден.' );
    return;
}

if ( $app->feedbackPost( $name, $email, $text ) === true )
    echo $app->preparedMessage( 'success', 'Ваш отзыв отправлен. Страница перезагрузится автоматически.' );
else
    echo $app->preparedMessage( 'danger', 'Что-то пошло не так. Отзыв не был отправлен.' );
// That's all. Bye!